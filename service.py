from connection import con, cur


def create_ingredient(name):
    cur.execute('INSERT INTO Ingredient VALUES(NULL, ?);', (name,))
    con.commit()


def create_drink(name, info, ingredients):
    cur.execute('INSERT INTO Drink VALUES(NULL, ?, ?);', (name, info,))
    ingredients_list = []
    for ingredient in ingredients:
        if not is_exist_ingredient(ingredient):
            create_ingredient(ingredient)
        ingredients_list.append(get_ingredient(ingredient))

    drink_id = get_drink_by_name(name)
    for ingredient_id in ingredients_list:
        cur.execute('INSERT INTO Recipe VALUES(NULL, ?, ?);', (drink_id, ingredient_id,))

    con.commit()


def get_ingredients():
    cur.execute('SELECT id, name FROM Ingredient')
    ingredients = map(lambda ingredient: ingredient['name'], cur.fetchall())
    return ingredients


def get_ingredient(name):
    cur.execute('SELECT id FROM Ingredient WHERE name = ?', (name,))
    return cur.fetchone()[0]


def get_ingredient_name_by_id(ingredient_id):
    cur.execute('SELECT name FROM Ingredient WHERE id = ?', (ingredient_id,))
    return cur.fetchone()[0]


def get_drink_by_name(name):
    cur.execute('SELECT id FROM Drink WHERE name = ?', (name,))
    return cur.fetchone()[0]


def is_exist_ingredient(name):
    cur.execute('SELECT id, name FROM Ingredient WHERE name = ?', (name,))
    return False if not cur.fetchall() else True


def get_all_drinks():
    cur.execute('SELECT id, name FROM Drink ORDER BY name ASC')
    return cur.fetchall()


def info_about_drink(name):
    cur.execute('SELECT id, name, info FROM Drink WHERE name = ?', (name,))
    drink = cur.fetchone()
    ingredients = [get_ingredient_name_by_id(ingredient['ingredient_id'])
                   for ingredient in get_ingredients_drink(drink['id'])]
    return {
        "id": drink[0],
        "name": drink[1],
        "info": drink[2],
        "ingredients": ingredients
    }


def get_ingredients_drink(drink_id):
    cur.execute('SELECT ingredient_id FROM Recipe WHERE drink_id = ?', (drink_id,))
    return cur.fetchall()


def get_drink_by_ingredient(ingredient_id):
    cur.execute('SELECT drink_id FROM Recipe WHERE ingredient_id = ?', (ingredient_id,))
    return cur.fetchall()


def get_drink_name_by_id(drink_id):
    cur.execute('SELECT name FROM Drink WHERE id = ?', (drink_id,))
    return cur.fetchone()[0]


def find_drinks_by_ingredients(ingredients):
    drinks = [get_drink_by_ingredient(ingredient)
              for ingredient in ingredients]

    names = []
    for drink in drinks:
        for item in drink[0]:
            names.append(get_drink_name_by_id(item))

    return list(set(names))
