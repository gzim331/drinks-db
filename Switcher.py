from service import *

inputElements = []


class Switcher(object):
    def indirect(self, operation):
        method = getattr(self, operation, lambda: 'Invalid')
        return method()

    def help(self):
        print('- help\n- add\n- drinks\n- ingredients\n- list\n- info\n- esc')

    def add(self):
        ingredients = []
        loopIngredients = True
        ingredientIterate = 1
        name = input('name: ')
        if not get_drink_by_name(name):
            print('Confirm each ingredient with <enter>. To finish, press enter or type esc')
            while loopIngredients:
                ingredientInput = input(f'Ingredient {ingredientIterate}: ')
                if ingredientInput == 'esc' or ingredientInput == '':
                    loopIngredients = False
                else:
                    ingredients.append(ingredientInput)
                    ingredientIterate += 1
            info = input('info: ')
            create_drink(name, info, ingredients)
        else:
            print('such a drink already exists')

    def drinks(self):
        print('Confirm each ingredient with <enter>. To finish, press enter or type esc')
        inputIngredients = []
        loopData = True
        while loopData:
            ingredientInput = input('Ingredient which you have: ')
            if ingredientInput == '':
                loopData = False
            elif ingredientInput == '?':
                self.ingredients()
            else:
                if is_exist_ingredient(ingredientInput):
                    inputIngredients.append(get_ingredient(ingredientInput))

        print('\n-----------------------')
        for drink in find_drinks_by_ingredients(inputIngredients):
            print(f"{drink}")
            [print(f"\t{ingredient}") for ingredient in info_about_drink(drink)['ingredients']]
        print('-----------------------')

    def ingredients(self):
        for ingredient in get_ingredients():
            print(ingredient)

    def list(self):
        for drink in get_all_drinks():
            print(f"{drink['id']}. {drink['name']}")

    def info(self):
        drinkNameInput = input('Drink name: ')
        print(drinkNameInput)
        drink = info_about_drink(drinkNameInput)
        print(f"{drink['id']}\n{drink['name']}\ninfo:\n{drink['info']}\ningredients:")
        for ingredient in drink['ingredients']:
            print(f"  {ingredient}")

    def esc(self):
        return 'esc'

    def __getattr__(self, item):
        print('Undefined command')
